import com.google.common.collect.Sets;
import com.mobiquityinc.packer.PackageItem;
import com.mobiquityinc.packer.Packer;
import org.junit.Test;
import org.mockito.Mock;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class OptimalPackageTest {

    @Mock
    Packer packer;

    @Test
    public void calculateOptimalPackageFromCorrectItemsCombinationTest() {
        Set<Set<PackageItem>>[] combinations = makeAllPossibleItemsCombinations(correctPackageItemsList());
        assertEquals("4", packer.calculateOptimalPackageContent(81, combinations));
    }

    @Test
    public void shouldReturnLighterPackageWhenPricesOfItemsAreSameTest() {
        Set<Set<PackageItem>>[] combinations = makeAllPossibleItemsCombinations(sameMaxPricesInPackageItemsList());
        assertEquals("2", packer.calculateOptimalPackageContent(99, combinations));
    }

    @Test
    public void shouldReturnDashIfNoItemFitsInPackageWeight() {
        Set<Set<PackageItem>>[] combinations = makeAllPossibleItemsCombinations(noItemsFitsMaxPackageWeightList());
        assertEquals("-", packer.calculateOptimalPackageContent(10.0, combinations));
    }

    private Set<PackageItem> correctPackageItemsList() {
        Set<PackageItem> correctItems = new LinkedHashSet<>();
        correctItems.add(new PackageItem(1, 53.38, 45));
        correctItems.add(new PackageItem(2, 88.62, 98));
        correctItems.add(new PackageItem(3, 78.48, 3));
        correctItems.add(new PackageItem(4, 72.3, 76));
        correctItems.add(new PackageItem(5, 30.18, 9));
        return correctItems;
    }

    private Set<PackageItem> sameMaxPricesInPackageItemsList() {
        Set<PackageItem> correctItems = new LinkedHashSet<>();
        correctItems.add(new PackageItem(1, 53.38, 45));
        correctItems.add(new PackageItem(2, 88.62, 98));
        correctItems.add(new PackageItem(3, 78.48, 3));
        correctItems.add(new PackageItem(4, 92.3, 98));
        correctItems.add(new PackageItem(5, 30.18, 9));
        return correctItems;
    }

    private Set<PackageItem> noItemsFitsMaxPackageWeightList() {
        Set<PackageItem> correctItems = new LinkedHashSet<>();
        correctItems.add(new PackageItem(1, 16.04, 80));
        return correctItems;
    }

    private static Set<Set<PackageItem>>[] makeAllPossibleItemsCombinations(Set<PackageItem> packageItems) {
        int numberOfItemsInPackage = packageItems.size() + 1;
        Set<Set<PackageItem>> combinations[] = new Set[numberOfItemsInPackage];
        for (int i = 0; i < numberOfItemsInPackage; i++) {
            combinations[i] = Sets.combinations(packageItems, i);
        }
        return combinations;
    }

}
