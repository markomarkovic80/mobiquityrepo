import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.fileUtils.FileParser;
import com.mobiquityinc.packer.PackageItem;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class FileParserTest {

    @Test
    public void shouldCreateCorrectItemsListFromRegularInputTest() throws APIException{
        Set<PackageItem> packageItems = new LinkedHashSet<>();
        int numberOfItems = FileParser.parseItemsFromInputFile(correctPackageItems(), packageItems);
        assertEquals(numberOfItems, 5);
        assertEquals(packageItems, correctPackageItemsList());
    }

    @Test(expected = APIException.class)
    public void shouldThrowAPIExceptionWhenItemWeightExceedsLimitTest() throws APIException{
        Set<PackageItem> packageItems = new LinkedHashSet<>();
        FileParser.parseItemsFromInputFile(itemWeightExceeded(), packageItems);
    }

    @Test(expected = APIException.class)
    public void shouldThrowAPIExceptionWhenItemPriceExceedsLimitTest() throws APIException{
        Set<PackageItem> packageItems = new LinkedHashSet<>();
        FileParser.parseItemsFromInputFile(itemPriceExceeded(), packageItems);
    }

    private String correctPackageItems() {
        return "(1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.3,€76) (5,30.18,€9)";
    }

    private String itemWeightExceeded() {
        return "(1,53.38,€45) (2,101.5,€98) (3,78.48,€3)";
    }

    private String itemPriceExceeded() {
        return "(1,53.38,€45) (2,88.62,€101) (3,78.48,€3)";
    }

    private Set<PackageItem> correctPackageItemsList() {
        Set<PackageItem> correctItems = new LinkedHashSet<>();
        correctItems.add(new PackageItem(1, 53.38, 45));
        correctItems.add(new PackageItem(2, 88.62, 98));
        correctItems.add(new PackageItem(3, 78.48, 3));
        correctItems.add(new PackageItem(4, 72.3, 76));
        correctItems.add(new PackageItem(5, 30.18, 9));
        return correctItems;
    }
}
