package com.mobiquityinc.fileUtils;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.PackageItem;
import com.mobiquityinc.validator.Validator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileParser {

    private final static String ITEM_PATTERN = "(\\d+,\\d+\\.\\d+,€\\d+)";

    /**
     * Extract items from a line of input file which is formatted like (1,53.38,€45) (2,88.62,€98)
     * and fills set of {@code PackageItem}s set
     *
     * @param  packageItemsInputFileLine package items from one line of input file
     * @param  packageItems set of {@code PackageItem}s filled with items from one line of input file
     * @return Number of items in a package
     * @throws APIException if number of items in a package or item weight or item value exceeds its limit
     */
    public static int parseItemsFromInputFile(String packageItemsInputFileLine, Set<PackageItem> packageItems) throws APIException {
        Pattern pattern = Pattern.compile(ITEM_PATTERN);
        Matcher packageItemsMatcher = pattern.matcher(packageItemsInputFileLine.trim());
        int groupCount = packageItemsMatcher.groupCount();
        int numberOfItemsInPackage = 0;
        while (packageItemsMatcher.find()) {
            for (int i = 1; i <= groupCount; i++) {
                String[] itemParts = packageItemsMatcher.group(i).split(",");
                int itemIndex = Integer.parseInt(itemParts[0]);
                double itemWeight = Double.parseDouble(itemParts[1]);
                int itemPrice = Integer.parseInt(itemParts[2].substring(1));
                Validator.validateItemPrice(itemPrice);
                Validator.validateItemWeight(itemWeight);
                packageItems.add(new PackageItem(itemIndex, itemWeight, itemPrice));
            }
            numberOfItemsInPackage++;
        }
        Validator.validateNumberOfItemsInPackage(numberOfItemsInPackage);
        return numberOfItemsInPackage;
    }

    /**
     * Reads each line of input file and stores it into a list
     *
     * @param filePath input file for processing
     * @return {@code List} of {@code String}s where each line of file is list member
     * @throws APIException if there were errors in file opening or reading
     */
    public static List<String> readInputFile(String filePath) throws APIException {
        final Path path = Paths.get(filePath);
        List<String> lineList;
        try {
            Stream<String> lines = Files.lines(path);
            lineList = lines.collect(Collectors.toList());
            lines.close();
        } catch (IOException e) {
            throw new APIException("There was an error opening a file specified as input parameter.");
        } catch (SecurityException se) {
            throw new APIException("There was a problem with read privilege on a file specified as input parameter.");
        }
        return lineList;
    }
}
