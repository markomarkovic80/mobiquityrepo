package com.mobiquityinc.validator;

import com.mobiquityinc.exception.APIException;

public class Validator {

    private static final double MAX_ITEM_WEIGHT = 100.0;
    private static final int MAX_ITEM_PRICE = 100;
    private static final int MAX_ITEMS_IN_PACKAGE = 15;
    private static final double MAX_PACKAGE_WEIGHT = 100.0;

    public static void validateItemWeight(double itemWeight) throws APIException {
        if (itemWeight > MAX_ITEM_WEIGHT) {
            throw new APIException("Package item weight cannot exceed " + MAX_ITEM_WEIGHT);
        }
    }

    public static void validateItemPrice(int itemPrice) throws APIException {
        if (itemPrice > MAX_ITEM_PRICE) {
            throw new APIException("Package item price cannot exceed " + MAX_ITEM_PRICE);
        }
    }

    public static void validateNumberOfItemsInPackage(int numberOfItemsInPackage) throws APIException {
        if (numberOfItemsInPackage > MAX_ITEMS_IN_PACKAGE) {
            throw new APIException("Package cannot contain more than " + MAX_ITEMS_IN_PACKAGE +" items.");
        }
    }

    public static void validatePackageWeight(double packageWeight) throws APIException {
        if (packageWeight > MAX_PACKAGE_WEIGHT) {
            throw new APIException("Package weight cannot be greater than " + MAX_PACKAGE_WEIGHT);
        }
    }

}
