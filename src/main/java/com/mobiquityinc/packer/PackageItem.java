package com.mobiquityinc.packer;

public class PackageItem {
    private int index;
    private double weight;
    private int price;

    public PackageItem(int index, double weight, int price) {
        this.index = index;
        this.weight = weight;
        this.price = price;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackageItem)) return false;

        PackageItem packageItem = (PackageItem) o;

        if (getIndex() != packageItem.getIndex()) return false;
        if (Double.compare(packageItem.getWeight(), getWeight()) != 0) return false;
        return getPrice() == packageItem.getPrice();
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getIndex();
        temp = Double.doubleToLongBits(getWeight());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + getPrice();
        return result;
    }

    @Override
    public String toString() {
        return "PackageItem{" +
                "index=" + index +
                ", weight=" + weight +
                ", price=" + price +
                '}';
    }
}
