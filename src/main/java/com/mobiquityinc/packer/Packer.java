package com.mobiquityinc.packer;

import com.google.common.collect.Sets;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.fileUtils.FileParser;
import com.mobiquityinc.validator.Validator;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Packer {

    private Packer() {
    }

    public static String pack(String filePath) throws APIException {
        List<String> lineList = FileParser.readInputFile(filePath);

        StringBuilder maxItemsString = new StringBuilder();
        for (String line : lineList) {
            String[] packageWeightAndItemsSeparation = line.trim().split(":");
            double projectedPackageWeight = Double.parseDouble(packageWeightAndItemsSeparation[0]);
            Validator.validatePackageWeight(projectedPackageWeight);

            Set<PackageItem> packageItems = new LinkedHashSet<>();
            int numberOfItemsInPackage = FileParser.parseItemsFromInputFile(packageWeightAndItemsSeparation[1], packageItems);

            //Produce all possible item combinations
            Set<Set<PackageItem>>[] combinations = makeAllPossibleItemsCombination(packageItems, numberOfItemsInPackage);

            String maxItems = calculateOptimalPackageContent(projectedPackageWeight, combinations);
            maxItemsString.append(maxItems).append("\n");
        }
        return maxItemsString.substring(0, maxItemsString.length() - 1);
    }

    private static Set<Set<PackageItem>>[] makeAllPossibleItemsCombination(Set<PackageItem> packageItems, int numberOfItemsInPackage) {
        Set<Set<PackageItem>> combinations[] = new Set[numberOfItemsInPackage + 1];
        for (int i = 0; i < numberOfItemsInPackage + 1; i++) {
            combinations[i] = Sets.combinations(packageItems, i);
        }
        return combinations;
    }

    /**
     * Iterates through all package items combinations and finds the optimal one
     * Optimal item combination is the one with maximal price that fits into projectedPackageWeight
     * If two item combinations have same price we pick one with less weight
     *
     * @param projectedPackageWeight Maximal weight of items combination
     * @param combinations           All possible item combinations
     * @return Optimal items combination or "-" if one does not exist
     */
    public static String calculateOptimalPackageContent(double projectedPackageWeight, Set<Set<PackageItem>>[] combinations) {
        int maxPrice = 0;
        String maxItems = "-";
        double maxItemMinWeight = 0.0;
        for (Set<Set<PackageItem>> combination : combinations) {
            for (Set<PackageItem> itemsCombination : combination) {
                int itemPrice = itemsCombination.stream()
                        .mapToInt(x -> x.getPrice())
                        .sum();
                double itemWeight = itemsCombination.stream()
                        .mapToDouble(x -> x.getWeight())
                        .sum();
                String currItems = itemsCombination.stream()
                        .map(item -> item.getIndex() + ", ")
                        .collect(Collectors.joining(""));
                if (currItems.length() > 0 && itemWeight <= projectedPackageWeight && itemPrice >= maxPrice) {
                    if (maxPrice > 0 && itemPrice == maxPrice) {
                        if (itemWeight < maxItemMinWeight) {
                            maxItems = currItems.substring(0, currItems.length() - 2);
                            maxItemMinWeight = itemWeight;
                        }
                    } else {
                        maxPrice = itemPrice;
                        maxItems = currItems.substring(0, currItems.length() - 2);
                        maxItemMinWeight = itemWeight;
                    }
                }
            }
        }
        return maxItems;
    }
}